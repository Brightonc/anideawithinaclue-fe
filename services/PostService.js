const postsUrl = '/posts'
const categoriesUrl = '/categories'

export default {
    getPost(id) {
        return this.$axios.get(`${postsUrl}/${id}`)
    },

    getPosts() { 
        console.log(this)
        return this.api.get(postsUrl)
    },
    
    getRecentPosts() {
        return this.$axios.get(postsUrl)
    },

    postNewPost(post) {
        return this.$axios.post(postsUrl, post)
    },

    putPost(id, post) {
        return this.$axios.put(`${postsUrl}/${id}`, post)
    },


    getCategories() {
        return this.$axios.get(categoriesUrl)
    },

    getCategory(id) {
        return this.$axios.get(`${categoriesUrl}/${id}`)
    }


}