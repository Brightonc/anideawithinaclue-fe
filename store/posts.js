
export const state = () => ({
  categories: [],
  selectedCategory: {},
  posts: [],
  selectedPost: {},
});

export const getters = {
  getPostById: (state) => (id) => {
    return state.posts.find((post) => post.id === id);
  },
  getCategoryById: (state) => (id) => {
    return state.categories.find((category) => category.id === id);
  },
};

export const mutations = {
  SET_CATEGORIES: (state, payload) => {
    state.categories = payload;
  },

  SET_CATEGORY: (state, payload) => {
    state.selectedCategory = payload;
  },

  SET_POSTS: (state, payload) => {
    state.posts = payload;
  },

  SET_POST: (state, payload) => {
    state.selectedPost = payload;
  },
};

export const actions = {
         fetchPosts({ commit }) {
      
           this.$axios
             .get('/posts')
             .then((res) => commit('SET_POSTS', res.data))
             .catch((error) => {
               console.log(error);
             });
         },

         fetchPost({ commit, getters, dispatch }, id) {
           const post = getters.getPostById(id);

           if (post) {
             commit('SET_POST', post);
           } else {
             return this.$axios.get('/posts/' + id)
               .then((res) => {
                 commit('SET_POST', res.data);
               })
               .catch((error) => {
                 const notification = {
                   type: 'ERROR',
                   message: 'There was a problem fetching post: ' + error,
                 };

                 dispatch('notification/push', notification, { root: true });
               });
           }
         },

         fetchCategories({ commit }) {
           this.$axios
             .get('/categories')
             .then((res) => commit('SET_CATEGORIES', res.data))
             .catch((error) => {
               console.log(error);
             });
         },

         fetchCategory({ commit, getters, dispatch }, id) {
           const cat = getters.getCategoryById(id);

           if (cat) {
             commit('SET_CATEGORY', cat);
           } else {
             this.$axios
               .get('/categories/' + id)
               .then((res) => {
                 commit('SET_CATEGORY', res.data);
               })
               .catch((error) => {
                 const notification = {
                   type: 'ERROR',
                   message: 'There was a problem fetching Category: ' + error,
                 };

                 dispatch('notification/push', notification, { root: true });
               });
           }
         },
       };
