export const state = () => ({
  infos: [],
  errors: [],
});

let nextId = 1;
export const mutations = {
  PUSH_INFO(state, info) {
    state.infos.push(...info, nextId++);
  },
  PUSH_ERROR(state, error) {
    state.errors.push(...error, nextId++);
  },

  DELETE_ERROR(state, errorToBeRemoved) {
    state.errors = state.errors.filter(
      (error) => error.id !== errorToBeRemoved.id
    );
  },

  DELETE_INFO(state, infoToBeRemoved) {
    state.infos = state.infos.filter((info) => info.id !== infoToBeRemoved.id);
  },
};

export const actions = {
  add({ commit }, notification) {
    if (notification.type === 'INFO') commit('PUSH_INFO', notification);
    else commit('PUSH_ERROR', notification);
  },

  remove({ commit }, notification) {
    if (notification.type === 'INFO') commit('DELETE_INFO', notification);
    else commit('DELETE_ERROR', notification);
  },
};
