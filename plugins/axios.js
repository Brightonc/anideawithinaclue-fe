export default function({ $axios }) {
  // Set baseURL to something different
  $axios.setBaseURL('https://anideawithinaclue-be.herokuapp.com/producer/alpha');
}
